const wraper = document.querySelector('main')

const characterCardWrap = 'character-wrap'
const characterCardWrapInfo = 'character-wrap-info'
const aboutCharacter = 'about-character'


 export default function characterCard ({image,name,species,location,created,episode}){
   
   
   const characterWrap = document.createElement('section')
   const characterWrapinfo = document.createElement('div')
   const inputImgg = document.createElement('img')
   const characterName = document.createElement('h1')
   const characterSpecies = document.createElement('p')
   const characterLocation = document.createElement('p')
   const characteCreated = document.createElement('p')
   const characteEpisode = document.createElement('p')

   const deletCharacter = document.createElement('button')

   let date = new Date(created)
   

   inputImgg.setAttribute('src', image)
   inputImgg.setAttribute('alt', name)
   inputImgg.setAttribute('title', name)
   inputImgg.className = 'characte-card-img'

   characterName.innerText = name
   characterName.className = aboutCharacter

   characterSpecies.innerText = `Species: ${species}`
   characterSpecies.className = aboutCharacter

   characterLocation.innerText = `Location: ${location.name}`
   characterLocation.className = aboutCharacter

   characteCreated.innerText = `Created: ${date.getFullYear()}-${date.getMonth()}-${date.getDay()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}`
   characteCreated.className = aboutCharacter

   characteEpisode.className = aboutCharacter
   characteEpisode.innerText = `Episods: ${episode.length}`

  
   deletCharacter.className = 'delet-character'
   deletCharacter.innerText = 'Delete character'
   characterWrapinfo.className = characterCardWrapInfo
   characterWrapinfo.append(characterName)
   characterWrapinfo.append(characterSpecies)
   characterWrapinfo.append(characterLocation)
   characterWrapinfo.append(characteCreated)
   characterWrapinfo.append(characteEpisode)
   characterWrapinfo.append(deletCharacter)
   
   
   characterWrap.className = characterCardWrap
   characterWrap.append(inputImgg)
   characterWrap.append(characterWrapinfo)
   
   wraper.append(characterWrap)
    
    
}

